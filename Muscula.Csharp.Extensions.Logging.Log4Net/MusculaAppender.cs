﻿using System;
using System.Linq;
using log4net.Appender;
using log4net.Core;
using Muscula.Csharp.Extensions.Logging.Exceptions;
using Muscula.Csharp.Extensions.Logging.Models;

namespace Muscula.Csharp.Extensions.Logging.Log4Net
{
    public sealed class MusculaAppender : AppenderSkeleton
    {
        public MusculaSettings MusculaSettings { get; set; }

        protected override void Append(LoggingEvent loggingEvent)
        {
            if (!MusculaProvider.IsConfigured)
            {
                MusculaProvider.Configure(new Uri(MusculaSettings.Url));
            }

            var stackTrace = StackTraceParser.Parse(loggingEvent.ExceptionObject?.StackTrace);

            MusculaProvider.EnqueueLog(new LogMessageDto
            {
                LogId = MusculaSettings.LogId,
                ClassName = loggingEvent.Repository.Name,
                Severity = MapSeverity(loggingEvent.Level),
                Exception = loggingEvent.ExceptionObject?.ToString(),
                Message = loggingEvent.ExceptionObject?.Message,
                FileName = stackTrace?.FileName,
                LineNumber = stackTrace?.LineNumber,
                StackTrace = stackTrace?.StackTraces.ToList(),
                Context = null,
                StructuralData = loggingEvent.ExceptionObject is MusculaException me ? me.StructuralData : null
            });
        }

        private static Severity? MapSeverity(Level level)
        {
            if (level == Level.Fatal)
            {
                return Severity.Fatal;
            }
            else if (level == Level.Error)
            {
                return Severity.Error;
            }
            else if (level == Level.Warn)
            {
                return Severity.Warning;
            }
            else if (level == Level.Info)
            {
                return Severity.Info;
            }
            else if (level == Level.Debug)
            {
                return Severity.Debug;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}
