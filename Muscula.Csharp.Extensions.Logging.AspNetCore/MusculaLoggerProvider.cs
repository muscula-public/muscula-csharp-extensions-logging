﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Muscula.Csharp.Extensions.Logging.AspNetCore
{
    public sealed class MusculaLoggerProvider : ILoggerProvider
    {
        private readonly Func<MusculaSettingsBuilder, MusculaSettings> _settingsFactory;
        private MusculaLogger _logger;

        public MusculaLoggerProvider(Func<MusculaSettingsBuilder, MusculaSettings> settingsFactory)
        {
            _settingsFactory = settingsFactory;
        }

        public void Dispose()
        {
            //ignore;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return _logger ??= new MusculaLogger(_settingsFactory(new MusculaSettingsBuilder()), categoryName);
        }
    }
}
