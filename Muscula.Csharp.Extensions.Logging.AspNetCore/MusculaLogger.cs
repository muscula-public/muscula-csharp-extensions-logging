﻿using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using Muscula.Csharp.Extensions.Logging.Exceptions;
using Muscula.Csharp.Extensions.Logging.Models;

namespace Muscula.Csharp.Extensions.Logging.AspNetCore
{
    public sealed class MusculaLogger : ILogger
    {
        public string CategoryName { get; }
        private readonly MusculaSettings _settings;

        public MusculaLogger(MusculaSettings settings, string categoryName)
        {
            _settings = settings;
            CategoryName = categoryName;
            MusculaProvider.Configure(new Uri(settings.Url ?? "https://harvester.muscula.com"));
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var stackTrace = StackTraceParser.Parse(exception?.StackTrace);

            MusculaProvider.EnqueueLog(new LogMessageDto
            {
                LogId = _settings.LogId,
                Severity = MapSeverity(logLevel),
                Exception = exception?.ToString(),
                Message = formatter(state, exception),
                FileName = stackTrace?.FileName,
                LineNumber = stackTrace?.LineNumber,
                StackTrace = stackTrace?.StackTraces.ToList(),
                ClassName = CategoryName,
                StructuralData = exception is MusculaException me ? me.StructuralData : null
            });
        }

        private static Severity? MapSeverity(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Trace:
                    return Severity.Trace;
                case LogLevel.Debug:
                    return Severity.Debug;
                case LogLevel.Information:
                    return Severity.Info;
                case LogLevel.Warning:
                    return Severity.Warning;
                case LogLevel.Error:
                    return Severity.Error;
                case LogLevel.Critical:
                    return Severity.Fatal;
                default:
                    throw new ArgumentOutOfRangeException(nameof(logLevel), logLevel, null);
            }
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
    }
}
