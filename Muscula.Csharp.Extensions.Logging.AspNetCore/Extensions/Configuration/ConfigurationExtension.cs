﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

namespace Muscula.Csharp.Extensions.Logging.AspNetCore.Extensions.Configuration
{
    public static class ConfigurationExtension
    {
        public static ILoggingBuilder AddMuscula(this ILoggingBuilder loggingBuilder, Func<MusculaSettingsBuilder, MusculaSettings> settingsFactory)
        {
            loggingBuilder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<ILoggerProvider, MusculaLoggerProvider>(
                p => new MusculaLoggerProvider(settingsFactory)));

            if (settingsFactory.Invoke(new MusculaSettingsBuilder()).StructuralLogger)
            {
                loggingBuilder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IMusculaStructuralLogger, MusculaStructuralLogger>(p => new MusculaStructuralLogger(settingsFactory)));
            }

            return loggingBuilder;
        }
    }
}
