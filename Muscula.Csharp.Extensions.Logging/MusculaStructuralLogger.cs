﻿using System;
using System.Linq;
using Muscula.Csharp.Extensions.Logging.Models;

namespace Muscula.Csharp.Extensions.Logging
{
    public class MusculaStructuralLogger : IMusculaStructuralLogger
    {
        private readonly MusculaSettings _settings;

        public MusculaStructuralLogger(Func<MusculaSettingsBuilder,MusculaSettings> settingsFactory)
        {
            _settings = settingsFactory(new MusculaSettingsBuilder());
        }
        
        public void LogStructural(Severity severity, string message = null, Exception exception = null, object structuralData = null, string className = null)
        {
            var stackTrace = StackTraceParser.Parse(exception?.StackTrace);
            
            MusculaProvider.EnqueueLog(new LogMessageDto
            {
                LogId = _settings.LogId,
                Severity = severity,
                Exception = exception?.ToString(),
                Message = exception?.Message ?? message,
                FileName = stackTrace?.FileName,
                LineNumber = stackTrace?.LineNumber,
                StackTrace = stackTrace?.StackTraces.ToList(),
                StructuralData = structuralData,
                ClassName = className ?? exception?.TargetSite?.DeclaringType?.FullName + '.' + exception?.TargetSite?.Name,
                Context = null
            });
        }
    }
}