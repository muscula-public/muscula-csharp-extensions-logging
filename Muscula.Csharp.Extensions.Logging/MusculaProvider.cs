﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Timers;
using Muscula.Csharp.Extensions.Logging.Models;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;

namespace Muscula.Csharp.Extensions.Logging
{
    public static class MusculaProvider
    {
        private const int NumberOfConcurrentWorkers = 3;
        private const double TimerInterval = 100;
        private const int MaxMisfireCount = 3;

        private static readonly ConcurrentQueue<LogMessageRequest> MessageRequests = new ConcurrentQueue<LogMessageRequest>();
        private static readonly object ConfigLock = new object();
        private static readonly Timer[] Timers = new Timer[NumberOfConcurrentWorkers];

        private static HttpClient _httpClient;
        private static bool _isRunning;

        public static bool IsConfigured { get; private set; }

        static MusculaProvider()
        {
            _httpClient = new HttpClient();
            for (var i = 0; i < NumberOfConcurrentWorkers; i++)
            {
                InitializeNewTimer(i);
            }
        }

        private static void InitializeNewTimer(in int i)
        {
            var timer = new Timer(TimerInterval);
            timer.Elapsed += TimerOnElapsed;
            timer.AutoReset = false;
            Timers[i] = timer;
        }

        private static void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            FireMessages();
            if (sender is Timer t)
            {
                t.Start();
            }
        }

        public static void EnqueueLog(LogMessageDto logMessageDto)
        {
            if (!IsConfigured)
                throw new InvalidOperationException(MusculaMessages.EndpointMissing);

            if (!_isRunning)
                StartWorkers();

            MessageRequests.Enqueue(logMessageDto.WrapToRequest());
        }

        private static void StartWorkers()
        {
            foreach (var timer in Timers)
            {
                timer.Start();
                Thread.Sleep((int)TimerInterval / NumberOfConcurrentWorkers);
            }
                
            _isRunning = true;
        }

        public static void Configure(Uri endpoint)
        {
            if (IsConfigured) return;
            lock (ConfigLock)
            {
                if (IsConfigured) return;
                _httpClient.BaseAddress = endpoint;
                IsConfigured = true;
            }
        }

        private static void FireMessages()
        {
            while (MessageRequests.TryDequeue(out var request))
            {
                var content = JsonConvert.SerializeObject(request.LogMessage, Formatting.None, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
#if DEBUG
                Debug.WriteLine(content);
#endif
                var result = _httpClient.PostAsync("/log",
                    new StringContent(content, Encoding.UTF8, "application/json")).Result;

                request.NumberOfRetries++;

#if DEBUG
                Debug.WriteLine($"Request result code {result.StatusCode}, message {result.Content.ReadAsStringAsync().Result}");
#endif
                
                if (result.StatusCode != HttpStatusCode.OK 
                    && result.StatusCode != HttpStatusCode.Accepted 
                    && result.StatusCode != HttpStatusCode.BadRequest 
                    && request.NumberOfRetries < MaxMisfireCount)
                {
                    MessageRequests.Enqueue(request);
                }
            }
        }
    }
}
