﻿namespace Muscula.Csharp.Extensions.Logging.Models
{
    public class LogMessageRequest
    {
        public LogMessageDto LogMessage { get; set; }
        
        public int NumberOfRetries { get; set; }
    }
}