﻿namespace Muscula.Csharp.Extensions.Logging.Models
{
    public enum Severity
    {
        Fatal,
        Error,
        Warning,
        Info,
        Debug,
        Trace
    }
}