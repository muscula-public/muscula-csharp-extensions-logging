﻿using System.Collections.Generic;

namespace Muscula.Csharp.Extensions.Logging.Models
{
    public class LogMessageDto
    {
        public string LogId { get; set; }
        public Severity? Severity { get; set; }
        public string Context { get; set; }
        public int? LineNumber { get; set; }
        public string FileName { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public List<string> StackTrace { get; set; }
        public string ClassName { get; set; }
        public object StructuralData { get; set; }

        public LogMessageRequest WrapToRequest() => new LogMessageRequest
        {
            LogMessage = this
        };
    }
}
