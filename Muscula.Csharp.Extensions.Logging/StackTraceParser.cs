﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Muscula.Csharp.Extensions.Logging
{
    public static class StackTraceParser
    {
        public static StackTrace Parse(string stackTrace)
        {
            if (string.IsNullOrWhiteSpace(stackTrace))
                return null;
            
            var st = stackTrace?.Split(" at ").Skip(1).ToList();
            string fileName = null;
            int? lineNumber = null;

            var firstTraceWithFileName = st?.FirstOrDefault(trace => trace.Contains(" in "));
            if (firstTraceWithFileName != null)
            {
                var fileNameLineNumber = firstTraceWithFileName.Trim().Split('\\').Last();
                fileName = fileNameLineNumber.Substring(0, fileNameLineNumber.IndexOf(":line", StringComparison.Ordinal));
                var lineNumberIndex = fileName.Length + 5;
                var lineNumberRaw = fileNameLineNumber.Substring(lineNumberIndex, fileNameLineNumber.Length - lineNumberIndex);

                if (int.TryParse(lineNumberRaw, out var l))
                {
                    lineNumber = l;
                } 
            }
            
            return new StackTrace{ StackTraces = st, FileName = fileName, LineNumber = lineNumber};
        }
    }

    public class StackTrace
    {
        public IEnumerable<string> StackTraces { get; set; }
        public string FileName { get; set; }
        public int? LineNumber { get; set; }
    }
}