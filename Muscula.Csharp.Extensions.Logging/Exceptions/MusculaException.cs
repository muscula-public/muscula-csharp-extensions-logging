﻿using System;

namespace Muscula.Csharp.Extensions.Logging.Exceptions
{
    public abstract class MusculaException : Exception
    {
        public readonly object StructuralData;

        protected MusculaException(string message, object structuralData) : base(message)
        {
            StructuralData = structuralData;
        }
    }
    
    public abstract class MusculaException<T> : MusculaException
    {
        protected MusculaException(string message, T structuralData) : base(message, structuralData)
        {
        }
    }
}