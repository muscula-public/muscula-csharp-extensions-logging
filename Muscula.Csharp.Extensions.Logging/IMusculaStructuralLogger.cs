﻿using System;
using Muscula.Csharp.Extensions.Logging.Models;

namespace Muscula.Csharp.Extensions.Logging
{
    public interface IMusculaStructuralLogger
    {
        void LogStructural(Severity severity, string message = null, Exception exception = null, object structuralData = null, string className = null);
    }
}