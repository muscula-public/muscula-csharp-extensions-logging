﻿namespace Muscula.Csharp.Extensions.Logging
{
    public class MusculaSettingsBuilder
    {
        private string _url;
        private string _logId;
        private bool _structuralLogger;

        public MusculaSettingsBuilder UseUrl(string url)
        {
            _url = url;
            return this;
        }

        public MusculaSettingsBuilder UseLogId(string logId)
        {
            _logId = logId;
            return this;
        }

        public MusculaSettingsBuilder UseStructuralLogger()
        {
            _structuralLogger = true;
            return this;
        }

        public MusculaSettings Build()
        {
            return new MusculaSettings
            {
                Url = _url,
                LogId = _logId,
                StructuralLogger = _structuralLogger
            };
        }
    }
}