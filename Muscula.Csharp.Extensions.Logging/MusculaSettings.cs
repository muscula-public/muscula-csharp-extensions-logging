﻿namespace Muscula.Csharp.Extensions.Logging
{
    public class MusculaSettings
    {
        public string Url { get; set; }

        public string LogId { get; set; }
        
        public bool StructuralLogger { get; set; }
    }
}