﻿namespace Muscula.Csharp.Extensions.Logging
{
    public static class MusculaMessages
    {
        public static string EndpointMissing => "Muscula endpoint configuration is missing.";
        public static string ProviderConfigured => "Muscula provider already configured.";
    }
}