﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Muscula.Csharp.Extensions.Logging.Exceptions;

namespace Muscula.Csharp.Extensions.Logging.Log4Net.Example.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            _logger.LogInformation("asdd");
            return Ok("ASD");
        }

        [HttpGet("throw")]
        public IActionResult Throw()
        {
            throw new ArgumentException($"Exception Content {HttpContext.TraceIdentifier}");
        }
        
        [HttpGet("throw-structural")]
        public IActionResult ThrowStructural()
        {
            throw new StructuralException("Exception Content", new Structural());
        }
    }
    
    public class StructuralException : MusculaException<Structural>
    {
        public StructuralException(string message, Structural structuralData) : base(message, structuralData)
        {
        }
    }

    public class Structural
    {
        public string[] Info = new[]
        {
            "t1",
            "t2",
            "t3"
        };
    }
}