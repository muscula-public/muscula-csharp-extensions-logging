# Muscula Csharp Extensions Logging For .Net Core. 

## Configuration for Microsoft Extensions Logging

In Startup.cs add 

```
services.AddLogging(builder => builder.AddMuscula(
    settingsBuilder =>
    {
        settingsBuilder.UseLogId(MusculaLogId);
        return settingsBuilder.Build();
    }));
```


You can check Muscula.Csharp.Extensions.Logging.AspNetCore.Example for example

## Configuration for Log4Net

In startup.cs add
```
            services.AddLogging(b => b.AddLog4Net("log4net.config"));
```

And provide log4net.config file
```
<?xml version="1.0" encoding="utf-8" ?>
<log4net>
    <appender name="MusculaAppender" type="Muscula.Csharp.Extensions.Logging.Log4Net.MusculaAppender, Muscula.Csharp.Extensions.Logging.Log4Net" >
        <layout type="log4net.Layout.PatternLayout">
            <conversionPattern value="%date [%thread] %-5level %logger [%ndc] - %message%newline" />
        </layout>
        <MusculaSettings>
            <LogId value="LOG_ID" />
        </MusculaSettings>
    </appender>
    <root>
        <level value="ALL" />
        <appender-ref ref="MusculaAppender" />
    </root>
</log4net>
```
