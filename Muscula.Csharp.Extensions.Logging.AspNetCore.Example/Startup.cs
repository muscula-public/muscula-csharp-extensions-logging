using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Muscula.Csharp.Extensions.Logging.AspNetCore.Extensions.Configuration;

namespace Muscula.Csharp.Extensions.Logging.AspNetCore.Example
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private static readonly string MusculaLogId = Environment.GetEnvironmentVariable("MusculaLogId");

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddLogging(builder => builder.AddMuscula(
                settingsBuilder => settingsBuilder.UseLogId(MusculaLogId)
                    .UseStructuralLogger()
                    .Build()));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
