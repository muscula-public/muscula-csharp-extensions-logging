﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Muscula.Csharp.Extensions.Logging.Exceptions;
using Muscula.Csharp.Extensions.Logging.Models;

namespace Muscula.Csharp.Extensions.Logging.AspNetCore.Example.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMusculaStructuralLogger _musculaLogger;

        public HomeController(ILogger<HomeController> logger, IMusculaStructuralLogger musculaLogger)
        {
            _logger = logger;
            _musculaLogger = musculaLogger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            _logger.LogInformation("asdd");
            return Ok("ASD");
        }

        [HttpGet("structural")]
        public IActionResult GetStructural()
        {
            try
            {
                throw new ArgumentException("Exception Content");
            }
            catch (Exception e)
            {
                _musculaLogger.LogStructural(Severity.Error, exception: e, structuralData: new
                {
                    HelperData = new
                    {
                        ProblematicValue = 343,
                        OtherValue = 32
                    }
                });
            }

            return Ok("ASD");
        }

        [HttpGet("/demodata")]
        public IActionResult LogDemoData()
        {
            var data = new DemoData();
            data.UserName = "Me";
            data.Count = 1;
            _musculaLogger.LogStructural(Severity.Trace, "User entered demo data section", structuralData: data);
            return Ok("ASD");
        }

        [HttpGet("throw")]
        public IActionResult Throw()
        {
            throw new ArgumentException("Exception Content");
        }

        [HttpGet("throw-structural")]
        public IActionResult ThrowStructural()
        {
            throw new StructuralException("Exception Content", new Structural());
        }
    }

    public class StructuralException : MusculaException<Structural>
    {
        public StructuralException(string message, Structural structuralData) : base(message, structuralData)
        {
        }
    }

    public class Structural
    {
        public string[] Info = new[]
        {
            "t1",
            "t2",
            "t3"
        };
    }
}
