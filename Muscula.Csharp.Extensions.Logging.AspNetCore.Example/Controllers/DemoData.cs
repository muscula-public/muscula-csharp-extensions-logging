namespace Muscula.Csharp.Extensions.Logging.AspNetCore.Example.Controllers
{
    public class DemoData
    {
        public string UserName { get; set; }
        public int Count { get; set; }
    }
}
