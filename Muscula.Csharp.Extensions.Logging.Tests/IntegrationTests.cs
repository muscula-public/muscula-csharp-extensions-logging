﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using Muscula.Csharp.Extensions.Logging.AspNetCore;
using Muscula.Csharp.Extensions.Logging.Models;
using NUnit.Framework;

namespace Muscula.Csharp.Extensions.Logging.Tests
{
    [TestFixture]
    public class IntegrationTests
    {
        private static readonly string MusculaLogId = Environment.GetEnvironmentVariable("MusculaLogId");
        private const string MusculaEndpoint = "https://harvester.muscula.com/";

        public IntegrationTests()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetExecutingAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory!, "log4net.config")));
        }

        [Test]
        public void MusculaProviderShouldFireMessages()
        {
            MusculaProvider.Configure(new Uri(MusculaEndpoint));
            var log = new LogMessageDto
            {
                LogId = MusculaLogId,
                LineNumber = 2,
                Message = "tsadaaauturutu",
                FileName = "assd",
                Exception = "exeex",
                StructuralData = new { AA = "asda", aaa = "123sa" }
            };
            MusculaProvider.EnqueueLog(log);

            while(true) {}
        }

        [Test]
        public void Log4NetShouldFireMessages()
        {
            var log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);

            log.Info("Message1");

            Exception e;

            try
            {
                throw new ArgumentException("Some exception message");
            }
            catch (Exception exception)
            {
                e = exception;
            }

            log.Fatal("Some exception message", e);

            Thread.Sleep(200);
        }

        [Test]
        public void MusculaLoggerShouldFireMessages()
        {
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var contextMock = new Mock<HttpContext>();
            var sessionMock = new Mock<ISession>();
            sessionMock.Setup(s => s.Id).Returns("session-id");
            contextMock.Setup(c => c.Session).Returns(sessionMock.Object);
            contextAccessorMock.Setup(c => c.HttpContext).Returns(contextMock.Object);
            var loggerProvider = new MusculaLoggerProvider(b =>
            {
                b.UseUrl(MusculaEndpoint);
                b.UseLogId(MusculaLogId);
                return b.Build();
            });

            var logger = loggerProvider.CreateLogger("");

            logger.Log(LogLevel.Information, new EventId(), "Message1");

            Exception e;

            try
            {
                throw new ArgumentException("Some exception message");
            }
            catch (Exception exception)
            {
                e = exception;
            }

            logger.Log(LogLevel.Critical, new EventId(), e, e.Message);

            Thread.Sleep(200);
        }
    }
}
