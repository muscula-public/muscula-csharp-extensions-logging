﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using log4net;
using log4net.Config;
using log4net.Repository.Hierarchy;
using Muscula.Csharp.Extensions.Logging.Log4Net;
using NUnit.Framework;

namespace Muscula.Csharp.Extensions.Logging.Tests
{
    [TestFixture]
    public class Log4NetTests
    {
        public Log4NetTests()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetExecutingAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));
        }
        
        [Test]
        public void ConfigShouldMapToProperInstance()
        {
            var log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);
            
            ((Logger) log.Logger).Parent.Appenders.ToArray().Should().Contain(ap => ap is MusculaAppender);
            ((MusculaAppender)((Logger) log.Logger).Parent.Appenders.ToArray().First(ap => ap is MusculaAppender))
                .MusculaSettings.Should().NotBeNull();
            
            ((MusculaAppender) ((Logger) log.Logger).Parent.Appenders.ToArray().First(ap => ap is MusculaAppender))
                .MusculaSettings.Url.Should().NotBeNullOrWhiteSpace();
            
            ((MusculaAppender)((Logger) log.Logger).Parent.Appenders.ToArray().First(ap => ap is MusculaAppender))
                .MusculaSettings.LogId.Should().NotBeNullOrWhiteSpace();
        }
    }
}